filepath=File.openDialog("Select a File"); 
img1 = File.getNameWithoutExtension(filepath);

// get default parameters from storage
numSpectra = parseInt(call("ij.Prefs.get",  "makeHDF5Spectra.numSpectra","1"));
backgr= parseInt(call("ij.Prefs.get",  "makeHDF5Spectra.backgr","0"));


Dialog.createNonBlocking("Enter number of spectra to load from file:");
Dialog.addNumber("Number of Spectra:", numSpectra) ;
Dialog.addNumber("subtract this from all:", backgr) ;
Dialog.show();
numSpectra=Dialog.getNumber();
backgr=Dialog.getNumber()+1;

hasBckgr = 0;
if (backgr >1) hasBckgr = 1;

//save new params to storage
call("ij.Prefs.set", "makeHDF5Spectra.numSpectra",toString(numSpectra )); 
call("ij.Prefs.set", "makeHDF5Spectra.backgr",toString(backgr-1)); 

x = 0;
prevw = 0;
prevh = 0;
isFirst = 1;
originallyOpen=nImages;
stackCounter = 0;


retrievalString = "";
frameCount=0;
for(y=0;y<numSpectra;y++) {
	if(y > 0) retrievalString += ",";
	if(y == 0){
		retrievalString += "/entry" + toString(y+1) + "/energy/data,";
		frameCount += 1;
	}
	retrievalString += "/entry" + toString(y+1) + "/counter0/data";
	frameCount += 1;
}

run("Scriptable load HDF5...", "load=" + filepath + " datasetnames=" + retrievalString + " nframes=" + toString(frameCount) + " nchannels=1");
rename("rawSpectraDataDuringScriptLoad");

lengthOfData = getHeight;
makeLine(0, 0, 0, lengthOfData);


//this section gets the individual line scans. it is taken from
// StackProfileData
// Version 1.0, 24-Sep-2010 Michael Schmid
setBatchMode(true);

run("Plot Profile");
Plot.getValues(x, y);
run("Clear Results");
close();

minX = 9999999999999999999;
maxX=-9999999999999999999;
minY=999999999999999999999;
maxY=-9999999999999999999;

n = nSlices;
counterXAS=0;

setSlice(backgr);
backgroundCounts = getProfile();

for (slice=1; slice<=n; slice++) {
	showProgress(slice, n);
	setSlice(slice);
	profile = getProfile();
	if (slice == 1) sliceLabel = "energy (eV)";
	else if (slice == backgr && hasBckgr) sliceLabel = "Backgr.";
	else{
		counterXAS+=1;
		sliceLabel = "XAS #" + toString(counterXAS);
	}
    
	sliceData = split(getMetadata("Label"),"\n");
	if (sliceData.length>0) {
		line0 = sliceData[0];
		if (lengthOf(sliceLabel) > 0) sliceLabel = sliceLabel+ " ("+ line0 + ")";
     	}
	for (i=0; i<profile.length; i++){
		if (hasBckgr){
			if (slice == 1){
				setResult(sliceLabel, i, profile[i]);
				if (minX>profile[i])  minX=profile[i];
				if (maxX<profile[i])  maxX=profile[i];
			}
			else if (slice != backgr){
				setResult(sliceLabel, i, abs(profile[i]/backgroundCounts[i]));
				if (minY>abs(profile[i]/backgroundCounts[i]))  minY=abs(profile[i]/backgroundCounts[i]);
				if (maxY<abs(profile[i]/backgroundCounts[i]))  maxY=abs(profile[i]/backgroundCounts[i]);
			}
		}
		else{
			setResult(sliceLabel, i, profile[i]);
			if (slice == 1){
				if (minX>profile[i])  minX=profile[i];
				if (maxX<profile[i])  maxX=profile[i];
			}
			else if (slice != backgr){
				if (minY>profile[i])  minY=profile[i];
				if (maxY<profile[i])  maxY=profile[i];
			}
		}

		
	}
 }
 setBatchMode(false);
 updateResults;
// end of copied part

selectWindow("rawSpectraDataDuringScriptLoad");
close();


// now plot the data
plotCount = 0;
legend = "";
plotColors = newArray("black", "blue", "red", "cyan", "darkGray", "gray", "green", "lightGray", "magenta", "orange", "pink", "yellow");
Plot.create("XAS_of_" + img1, "energy (eV)", "XAS");
if (hasBckgr) n-=1;

for (slice=1; slice<=n; slice++) {
	if (slice == 1) continue;
	if (slice == backgr && hasBckgr) continue;
		
	
	Plot.add("Line", Table.getColumn("energy (eV)", "Results"), Table.getColumn("XAS #"+toString(slice-1), "Results"));
	Plot.setStyle(plotCount, plotColors[plotCount]+",#a0a0ff,1.0,Line");

	if (plotCount >0) legend += "\n";
	legend += "XAS #"+toString(plotCount+1);

	plotCount += 1;
}
Plot.addLegend(legend , "Auto Transparent");
Plot.setLogScaleX(false);
Plot.setLogScaleY(false);
Plot.setLimits(minX,maxX,minY,maxY);


print("finished processing");
