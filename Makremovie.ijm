date="2021-02-12";

startImage = 001;
stopImage = 001;

frameRate = 10;
saveVideos = 1;
removeBackground = 1;


for (i=startImage; i<=stopImage; i++) {


	if (i<10){
		number = "00"+d2s(i,0);
	}
	else{
		if (i<100){
			number = "0"+d2s(i,0);
		}
		else{
			number=d2s(i,0);
		}
	}

	run("Close All");

	run("Image Sequence...", "open=Z:\\Data1\\"+date+"\\Analysed\\Time_Resoved_Image_"+date+"_"+number+"\\Time_Resolved_Image_"+date+"_"+number+"_Time_1.tif sort");

	run("Rotate 90 Degrees Left");

	run("Z Project...", "projection=[Average Intensity]");
	imageCalculator("Divide create 32-bit stack", "Time_Resoved_Image_"+date+"_"+number+"","AVG_Time_Resoved_Image_"+date+"_"+number+"");
	//run("Enhance Contrast...", "saturated=0.3 normalize process_all");

	selectWindow("Result of "+"Time_Resoved_Image_"+date+"_"+number);
	rename("TR");

	if (removeBackground){
		run("Duplicate...", "duplicate");
		rename("BKG");
		run("Gaussian Blur...", "sigma=20 stack");
		imageCalculator("Subtract create 32-bit stack", "TR","BKG");
		selectWindow("TR");
		close();
		selectWindow("BKG");
		close();
		selectWindow("Result of TR");
	}

	if (saveVideos) {
		run("AVI... ", "compression=Uncompressed frame="+d2s(frameRate,0)+" save=[Z:\\Data1\\"+date+"\\Analysed\\Normalized Time_Resoved_Image_"+date+"_"+number+".avi]");
		saveAs("Tiff","Z:\\Data1\\"+date+"\\Analysed\\Normalized Time_Resoved_Image_"+date+"_"+number+".tif");
		run("RGB Color");
		saveAs("Gif", "Z:\\Data1\\"+date+"\\Analysed\\Normalized Time_Resoved_Image_"+date+"_"+number+".gif");
	}



	selectWindow("Time_Resoved_Image_"+date+"_"+number+"");

	if (saveVideos) {
		run("AVI... ", "compression=Uncompressed frame="+d2s(frameRate,0)+" save=[Z:\\Data1\\"+date+"\\Analysed\\Time_Resoved_Image_"+date+"_"+number+".avi]");
		saveAs("Tiff", "Z:\\Data1\\"+date+"\\Analysed\\Time_Resoved_Image_"+date+"_"+number+".tif");
		run("RGB Color");
		saveAs("Gif", "Z:\\Data1\\"+date+"\\Analysed\\Time_Resoved_Image_"+date+"_"+number+".gif");
	}

	selectWindow("AVG_Time_Resoved_Image_"+date+"_"+number+"");
	saveAs("PNG", "Z:\\Data1\\"+date+"\\Analysed\\AVG_Time_Resoved_Image_"+date+"_"+number+".png");
	saveAs("Jpeg", "Z:\\Data1\\"+date+"\\Analysed\\AVG_Time_Resoved_Image_"+date+"_"+number+".jpg");

	if (saveVideos) {
		selectWindow("Normalized Time_Resoved_Image_"+date+"_"+number+".tif");
	}
	else {
		selectWindow("Result of TR");
	}

	run("Close All");
}


// Opens last image and plays animation
open("Z:\\Data1\\"+date+"\\Analysed\\Time_Resoved_Image_"+date+"_"+number+".tif");
open("Z:\\Data1\\"+date+"\\Analysed\\Normalized Time_Resoved_Image_"+date+"_"+number+".tif");

doCommand("Start Animation [\\]");
