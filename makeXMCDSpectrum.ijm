filepath1=File.openDialog("Select first polarization"); 
filepath2=File.openDialog("Select second polarization"); 

img1 = File.getNameWithoutExtension(filepath1);
img2 = File.getNameWithoutExtension(filepath2);

auswahl=newArray(""+img1+"",""+img2+"");

// get default parameters from storage
numSpectra = parseInt(call("ij.Prefs.get",  "makeHDF5Spectra.numSpectra","1"));
startNum = parseInt(call("ij.Prefs.get",  "makeHDF5Spectra.startNum","1"));
endNum = parseInt(call("ij.Prefs.get",  "makeHDF5Spectra.endNum","-1"));
doNorm = parseInt(call("ij.Prefs.get",  "makeHDF5Spectra.doNorm","1"));

Dialog.createNonBlocking("Enter the polarization");
Dialog.addChoice("Enter the right(+) circularly polarized image",auswahl,""+img1+"");
Dialog.addChoice("Enter the left(-) circularly polarized image",auswahl,""+img2+"");
Dialog.addNumber("Number of Spectra:", numSpectra) ;
Dialog.addNumber("Start at index:", startNum) ;
Dialog.addNumber("End at index:", endNum) ;
Dialog.addCheckbox("Perform normalization?", true);
Dialog.show();

img1 = Dialog.getChoice();
img2 = Dialog.getChoice();
numSpectra=Dialog.getNumber();
startNum=Dialog.getNumber();
endNum=Dialog.getNumber();
doNorm=Dialog.getCheckbox();

//save new params to storage
call("ij.Prefs.set", "makeHDF5Spectra.numSpectra",toString(numSpectra )); 
call("ij.Prefs.set", "makeHDF5Spectra.startNum",toString(startNum )); 
call("ij.Prefs.set", "makeHDF5Spectra.endNum",toString(endNum )); 
call("ij.Prefs.set", "makeHDF5Spectra.doNorm",toString(doNorm )); 

x = 0;
retrievalString = "";
frameCount=0;
for(y=0;y<numSpectra;y++) {
	if(y > 0) retrievalString += ",";
	if(y == 0){
		retrievalString += "/entry" + toString(y+1) + "/energy/data,";
		frameCount += 1;
	}
	retrievalString += "/entry" + toString(y+1) + "/counter0/data";
	frameCount += 1;
}

run("Scriptable load HDF5...", "load=" + filepath1 + " datasetnames=" + retrievalString + " nframes=" + toString(frameCount) + " nchannels=1");
rename("im1");
lengthOfData1 = getHeight;
run("Scriptable load HDF5...", "load=" + filepath2 + " datasetnames=" + retrievalString + " nframes=" + toString(frameCount) + " nchannels=1");
rename("im2");
lengthOfData2 = getHeight;

if (lengthOfData2 != lengthOfData1){
	print("lengths of spectra do not match!");
	return;
}

if (endNum!=-1 && endNum < startNum){
	print("start and end of selection incorrect!");
	return;
}

run("Concatenate...", "  image1=im1 image2=im2");;
rename("im1im2");
makeLine(0, 0, 0, lengthOfData2);


//this section gets the individual line scans. it is taken from
// StackProfileData
// Version 1.0, 24-Sep-2010 Michael Schmid
setBatchMode(true);

run("Plot Profile");
Plot.getValues(x, y);
run("Clear Results");
close();

minX = 9999999999999999999;
maxX=-9999999999999999999;
minY=999999999999999999999;
maxY=-9999999999999999999;

n = numSpectra+1;
counterXMCD=startNum-1;
for (slice=1; slice<=n; slice++) {
	if ((slice != 1 && slice < startNum+1) || (endNum != -1 && slice > endNum+1)) continue;
	showProgress(slice, n);
	setSlice(slice+numSpectra+1);
	profileRef = getProfile();
	setSlice(slice);
	profile = getProfile();
	if (slice == 1) sliceLabel = "energy (eV)";
	else{
		sliceLabel = "XMCD #" + toString(counterXMCD%numSpectra+1);
		counterXMCD+=1;
	}
    
	sliceData = split(getMetadata("Label"),"\n");
	if (sliceData.length>0) {
		line0 = sliceData[0];
		if (lengthOf(sliceLabel) > 0) sliceLabel = sliceLabel+ " ("+ line0 + ")";
     	}
	for (i=0; i<profile.length; i++){
		if (slice == 1) value = profile[i];
		else 
		{
			if (doNorm) value = (profile[i]-profileRef[i])/(profile[i]+profileRef[i]);
			else value = (profile[i]-profileRef[i]);
		}

		setResult(sliceLabel, i, value);
		
		if (slice == 1){
			if (minX>value)  minX=value;
			if (maxX<value)  maxX=value;
		}
		else{
			if (minY>value)  minY=value;
			if (maxY<value)  maxY=value;
		}

		
	}
 }
 setBatchMode(false);
 updateResults;
// end of copied part

selectWindow("im1im2");
close();


// now plot the data
plotCount = startNum-1;
legend = "";
plotColors = newArray("black", "blue", "red", "cyan", "darkGray", "gray", "green", "lightGray", "magenta", "orange", "pink", "yellow");
Plot.create("XMCD_of_" + img1 + "_and_" + img2, "energy (eV)", "XMCD");
actPlotCount=0;
for (slice=2; slice<=n; slice++) {
	if ((slice != 1 && slice < startNum+1) || (endNum != -1 && slice > endNum+1)) continue;	
	
	Plot.add("Line", Table.getColumn("energy (eV)", "Results"), Table.getColumn("XMCD #"+toString(slice-1), "Results"));
	Plot.setStyle(actPlotCount, plotColors[plotCount]+",#a0a0ff,1.0,Line");

	if (actPlotCount >0) legend += "\n";
	legend += "XMCD #"+toString(plotCount+1);

	plotCount += 1;
	actPlotCount += 1;
}
Plot.addLegend(legend , "Auto Transparent");
Plot.setLogScaleX(false);
Plot.setLogScaleY(false);
Plot.setLimits(minX,maxX,minY,maxY);


print("finished processing");
